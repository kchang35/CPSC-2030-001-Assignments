/*select top 10 bottom by speed */

SELECT *
FROM pokemon
ORDER BY speed DESC
LIMIT 10;

/*speed and ground */

SELECT pname
FROM pokemon, pokemon_res, pokemon_vul
WHERE resist = `steel` AND vulnerable = `ground`;

/*bst 200 to 500 weak to water */

SELECT pname
FROM pokemon, pokemon_weak
WHERE BST > 200 AND BST < 500 AND weakness = `water`;

/*highest mega and vulnerable to fire */

SELECT pname, MAX(BST)
FROM pokemon, pokemon_vul
WHERE mega = 1 AND vulnerable = `fire`;

